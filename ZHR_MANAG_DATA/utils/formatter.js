/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([], function () {
	"use strict";
	return {
		formatImageURL: function (m) {
			var u = "";
			if (m && typeof m === "string") {
				var l = document.createElement("a");
				l.href = m;
				u = (l.pathname.charAt(0) === "/") ? l.pathname : "/" + l.pathname;
			}
			return u;
		},
		formatObjectTextWithBrackets: function (t, T) {
			if (T) {
				return t + " (" + T + ")";
			}
			return t;
		},
		formatObjectTitle: function (n, e) {
			if (sap.ui.Device.system.desktop && e) {
				return n + " (" + e + ")";
			}
			return n;
		},
		formatDirectReportsCountNumber: function (n) {
			if (n === 0) {
				return null;
			} else {
				return n;
			}
		},
		formatDirectReportsCountUnit: function (n) {
			if (n === 0) {
				return this.getModel("i18n").getResourceBundle().getText("noDirectReports");
			} else {
				return n > 1 ? this.getModel("i18n").getResourceBundle().getText("directReports") : this.getModel("i18n").getResourceBundle().getText(
					"directReport");
			}
		},
		formatManagerLink: function (m) {
			if (m) {
				var a = this.getRouter().getURL("employeeDetails", {
					EmployeeNumber: m
				});
				var c = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
				return c.hrefForAppSpecificHash(a);
			}
			return null;
		},
		formatOfficeLocation: function (b, r) {
			if (b && r) {
				return b + ", " + r;
			} else if (b && !r) {
				return b;
			} else if (!b && r) {
				return r;
			} else {
				return null;
			}
		}
	};
});