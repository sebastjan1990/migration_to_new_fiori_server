/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([],
	function() {
		"use strict";

		return {

			onPhoneClick: function(oEvent) {
				sap.m.URLHelper.triggerTel(oEvent.getSource().getText());
			},

			onEmailClick: function(oEvent) {
				sap.m.URLHelper.triggerEmail(oEvent.getSource().getText());
			}
		};
	});