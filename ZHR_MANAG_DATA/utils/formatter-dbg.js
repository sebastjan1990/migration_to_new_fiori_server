/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([],
	function() {
		"use strict";

		return {

			/**
			 * extracts the service path from the given absolute URL
			 * @public
			 * @param {string} sURL absolute URL
			 * @returns {string} service path
			 */
			formatImageURL: function(sMediaSrc) {
				var sUrl = "";
				if (sMediaSrc && typeof sMediaSrc === "string") {
					var oLink = document.createElement("a");
					oLink.href = sMediaSrc;
					sUrl = (oLink.pathname.charAt(0) === "/") ? oLink.pathname : "/" + oLink.pathname;
				}
				return sUrl;
			},

			formatObjectTextWithBrackets: function(sText, sTextInBrackets) {
				if (sTextInBrackets) {
					return sText + " (" + sTextInBrackets + ")";
				}
				return sText;
			},

			formatObjectTitle: function(sName, sEmployeeID) {
				if (sap.ui.Device.system.desktop && sEmployeeID) {
					return sName + " (" + sEmployeeID + ")";
				}
				return sName;
			},

			formatDirectReportsCountNumber: function(sNoOfDirectReports) {
				if (sNoOfDirectReports === 0) {
					return null;
				} else {
					return sNoOfDirectReports;
				}
			},

			formatDirectReportsCountUnit: function(sNoOfDirectReports) {
				if (sNoOfDirectReports === 0) {
					return this.getModel("i18n").getResourceBundle().getText("noDirectReports");
				} else {
					return sNoOfDirectReports > 1 ? this.getModel("i18n").getResourceBundle().getText("directReports") : this.getModel("i18n").getResourceBundle()
						.getText("directReport");
				}
			},

			formatManagerLink: function(sManagerID) {
				if (sManagerID) {
					var sAppHashToMyRoute = this.getRouter().getURL("employeeDetails", {
						EmployeeNumber: sManagerID
					});
					var oCrossAppNavigator = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
					return oCrossAppNavigator.hrefForAppSpecificHash(sAppHashToMyRoute);
				}
				return null;
			},

			formatOfficeLocation: function(building, room) {
				if (building && room) {
					return building + ", " + room;
				} else if (building && !room) {
					return building;
				} else if (!building && room) {
					return room;
				} else {
					return null;
				}
			}
		};
	});