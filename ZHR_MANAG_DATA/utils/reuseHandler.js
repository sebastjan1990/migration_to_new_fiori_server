/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([],function(){"use strict";return{onPhoneClick:function(e){sap.m.URLHelper.triggerTel(e.getSource().getText());},onEmailClick:function(e){sap.m.URLHelper.triggerEmail(e.getSource().getText());}};});
