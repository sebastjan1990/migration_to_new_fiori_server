/*global location */
sap.ui.define([
		"petrol/hr/ManagerProfile/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"petrol/hr/ManagerProfile/model/formatter"
	], function (BaseController, JSONModel, formatter) {
		"use strict";

		return BaseController.extend("petrol.hr.ManagerProfile.controller.DetailDocuments", {
			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				
				jQuery.sap.registerModulePath("petrol/hr/Document", "/sap/bc/ui5_ui5/sap/zhr_document/");

				this.getRouter().getRoute("document").attachPatternMatched(this._onObjectMatched, this);
				
				
			},
			
			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */
			
			onNavBackDetails : function() {
				
				this.getRouter().navTo("object", {objectId : this._sObjectId}, true);
				// this.getOwnerComponent()._oSplitApp.backDetail();
			},
			
			/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */

			/**
			 * Binds the view to the object path and expands the aggregated line items.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
				var oPage = this.byId("pageDocuments");
				var sObjectId =  oEvent.getParameter("arguments").objectId;
				this._sObjectId = sObjectId;
	// Open external app with documents
				if (!this.getOwnerComponent()._oComp) {
					this.getOwnerComponent()._oComp =  new sap.m.Shell({
						app : new sap.ui.core.ComponentContainer({
						height : "100%",
						name : "petrol.hr.Document"
						}),
						appWidthLimited: false
					}).placeAt(oPage);
				};
			},

			/**
			 * Binds the view to the object path. Makes sure that detail view displays
			 * a busy indicator while data for the corresponding element binding is loaded.
			 * @function
			 * @param {string} sObjectPath path to the object to be bound to the view.
			 * @private
			 */
			_bindView : function (sObjectPath) {
				// Set busy indicator during view binding
				var oViewModel = this.getModel("detailView");

				// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				oViewModel.setProperty("/busy", false);

				this.getView().bindElement({
					path : sObjectPath,
					events: {
						change : this._onBindingChange.bind(this),
						dataRequested : function () {
							oViewModel.setProperty("/busy", true);
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
			},

			_onBindingChange : function () {
				var oView = this.getView(),
					oElementBinding = oView.getElementBinding();

				// No data for the binding
				if (!oElementBinding.getBoundContext()) {
					this.getRouter().getTargets().display("detailObjectNotFound");
					// if object could not be found, the selection in the master list
					// does not make sense anymore.
					this.getOwnerComponent().oListSelector.clearMasterListSelection();
					return;
				}

				var sPath = oElementBinding.getPath(),
					oResourceBundle = this.getResourceBundle(),
					oObject = oView.getModel().getObject(sPath),
					sObjectId = oObject.PersonnelNo,
					sObjectName = oObject.LastName,
					oViewModel = this.getModel("detailView");
			},

		});

	}
);