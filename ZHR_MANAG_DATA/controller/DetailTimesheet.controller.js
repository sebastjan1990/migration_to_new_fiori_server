sap.ui.define([
		"petrol/hr/ManagerProfile/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("petrol.hr.ManagerProfile.controller.DetailTimesheet", {
			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			onInit : function () {
				this.getRouter().getRoute("timesheet").attachPatternMatched(this._onObjectMatched, this);
				
				var oMaxDate = new Date();
				oMaxDate.setUTCHours(0,0,0,0);
				
				var oViewModel = new sap.ui.model.json.JSONModel({
					maxDateOtherPeriod: oMaxDate,
					OtherPeriodSelected: false,
					selectedIndex: 1,
					otherPeriodValueState: null,
					otherPeriodValue: undefined,
					EmployeeId: null,
					oOtherPeriodMessage: null
				});
					var sServiceUrl = "/sap/opu/odata/sap/ZHR_MY_PERSONAL_DATA_SRV/";
			var oModel = new sap.ui.model.odata.v2.ODataModel(sServiceUrl, false);
			this.setModel(oModel);
			
				this.getView().setModel(oViewModel, "viewModel");
			},
			
			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */
			
			onNavBackDetails : function() {
				this.getRouter().navTo("object", {objectId : this._sObjectId}, true);
			},
			
			/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */
			
			/**
			 * Binds the view to the object path and expands the aggregated line items.
			 * @function
			 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
			 * @private
			 */
			_onObjectMatched : function (oEvent) {
				this._sObjectId = oEvent.getParameter("arguments").objectId;
				
				var oViewModel = this.getView().getModel("viewModel");
				oViewModel.setProperty("/EmployeeId", this._sObjectId);
			},
			
			onPeriodChange : function() {
			
				var oView = this.getView();
				var oViewModel = oView.getModel("viewModel");
				
				if(oViewModel.getProperty("/otherPeriodValueState") === "Error") {
					
					var oDateRange = this.byId("OtherPeriodRange");
					
					setTimeout(function() { oDateRange.onChange(); }, 0);
					
				}
			
			},
			
			onDownloadStatement : function() {
				var oView = this.getView();
				var oTimeSheetModel = oView.getModel();
				var oViewModel = oView.getModel("viewModel");
				var iSelectedIndex = oViewModel.getProperty("/selectedIndex");
				
				var oStartDate, oEndDate;
				
				if(iSelectedIndex === 0) {
				
					oStartDate = new Date();
					oStartDate.setUTCHours(0,0,0,0);
					oStartDate.setDate(oStartDate.getDate() - 7);
					
					oEndDate = new Date();
					oEndDate.setUTCHours(0,0,0,0);
				
				} else if(iSelectedIndex === 1) {
				
					oStartDate = new Date();
					oStartDate.setUTCHours(0,0,0,0);
					
					oStartDate.setDate(1);
					
					oEndDate = new Date();
					oEndDate.setUTCHours(0,0,0,0);
				
				} else {
				
					var oDateRange = this.byId("OtherPeriodRange");
					
					if(oViewModel.getProperty("/otherPeriodValueState") === "Error") {
						
						setTimeout(function() {
							oDateRange.focus();	
						}, 0);
						
						return;
					}
					
					oStartDate = oDateRange.getDateValue();
					oEndDate = oDateRange.getSecondDateValue();
					
					var iOffset = oStartDate.getTimezoneOffset();
					
					oStartDate.setMinutes(oStartDate.getMinutes() - iOffset);
					oEndDate.setMinutes(oEndDate.getMinutes() - iOffset);
				
				}
				
				var sTimeStatementUrl = oTimeSheetModel.createKey("/TimeStatementSet", {
					EmployeeID: oViewModel.getProperty("/EmployeeId"),
					Begda: oStartDate,
					Endda: oEndDate
				});
				
	        	sTimeStatementUrl = this.getOwnerComponent().getModel().sServiceUrl + sTimeStatementUrl + "/$value";
	        	
	        	window.open(sTimeStatementUrl, "_blank");
			}
		});

	}
);