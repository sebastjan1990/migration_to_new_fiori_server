/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["petrol/hr/ManagerProfile/controller/BaseController", "sap/ui/model/json/JSONModel"], function (B, J) {
	"use strict";
	return B.extend("petrol.hr.ManagerProfile.controller.App", {
		onInit: function () {
			var v, s, o = this.getView().getBusyIndicatorDelay();
			v = new J({
				busy: true,
				delay: 0
			});
			var sServiceUrl = "/sap/opu/odata/sap/HCMFAB_EMPLOYEELOOKUP_SRV/";
			var oModel = new sap.ui.model.odata.v2.ODataModel(sServiceUrl, false);
			this.setModel(oModel);
			this.setModel(v, "appView");
			s = function () {
				v.setProperty("/busy", false);
				v.setProperty("/delay", o);
			};
			this.getOwnerComponent().getModel().metadataLoaded().then(s);
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		}
	});
});