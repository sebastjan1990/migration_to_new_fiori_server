/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["sap/ui/base/Object", "sap/m/MessageBox"], function (U, M) {
	"use strict";

	function E(r) {
		var a, o = {
			sMessage: r,
			sDetails: null,
			aInnerErrors: []
		};
		try {
			a = JSON.parse(r);
			if (a && a.error) {
				if (a.error.message && a.error.message.value) {
					o.sMessage = a.error.message.value;
				}
				if (a.error.code) {
					o.sDetails = a.error.code;
				}
				if (a.error.innererror && a.error.innererror.errordetails) {
					o.aInnerErrors = a.error.innererror.errordetails;
				}
			}
		} catch (e) {
			try {
				var x = jQuery.parseXML(r);
			} catch (f) {
				jQuery.sap.log.error(f);
			}
			if (x) {
				o.sMessage = x.getElementsByTagName("message")[0].childNodes[0].nodeValue || x.documentElement;
				o.sDetails = x.getElementsByTagName("code")[0].childNodes[0].nodeValue;
				o.aInnerErrors = x.getElementsByTagName("errordetail");
			} else {
				o.sMessage = r;
			}
		}
		return o;
	}

	function p(e) {
		var P = null,
			r = null;
		P = e.getParameters ? e.getParameters() : null;
		r = P ? (P.response || P) : e;
		var a = r.responseText || r.body || (r.response && r.response.body) || "";
		return E(a);
	}
	return U.extend("petrol.hr.ManagerProfile.controller.ErrorHandler", {
		constructor: function (c) {
			this._oResourceBundle = c.getModel("i18n").getResourceBundle();
			this._oComponent = c;
			this._oModel = c.getModel();
			this._sErrorText = this._oResourceBundle.getText("errorTitle");
			this._oModel.attachMetadataFailed(function (e) {
				var P = e.getParameters();
				this._showMetadataError(P.response);
			}, this);
			this._oModel.attachRequestFailed(this.onRequestFailed, this);
		},
		onRequestFailed: function (e) {
			var o = p(e);
			this._showServiceError(o.sMessage, o.sDetails);
		},
		showErrorMsg: function (e, o) {
			var a = p(e);
			M.show(a.sMessage, {
				icon: M.Icon.ERROR,
				title: this._oResourceBundle.getText("errorTitle"),
				details: a.sDetails,
				actions: M.Action.CLOSE,
				onClose: o,
				styleClass: this._oComponent.getContentDensityClass()
			});
		},
		_showMetadataError: function (d) {
			M.error(this._sErrorText, {
				id: "metadataErrorMessageBox",
				details: d,
				styleClass: this._oComponent.getContentDensityClass(),
				actions: [M.Action.RETRY, M.Action.CLOSE],
				onClose: function (a) {
					if (a === M.Action.RETRY) {
						this._oModel.refreshMetadata();
					}
				}.bind(this)
			});
		},
		_showServiceError: function (e, s, t) {
			if (this._bMessageOpen) {
				return;
			}
			this._bMessageOpen = true;
			M.error(e, {
				id: "serviceErrorMessageBox",
				title: t ? t : this._sErrorText,
				details: s,
				styleClass: this._oComponent.getContentDensityClass(),
				actions: [M.Action.CLOSE],
				onClose: function () {
					this._bMessageOpen = false;
				}.bind(this)
			});
		}
	});
});