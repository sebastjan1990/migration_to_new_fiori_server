sap.ui.define(["petrol/hr/ManagerProfile/controller/BaseController", "sap/ui/model/json/JSONModel", "sap/ui/core/routing/History",
	"petrol/hr/ManagerProfile/utils/formatter", "petrol/hr/ManagerProfile/utils/reuseHandler", "sap/ui/core/format/DateFormat",
	"hcm/fab/lib/common/util/CommonModelManager"
], function (B, J, H, f, r, D, C) {
	"use strict";
	return B.extend("petrol.hr.ManagerProfile.controller.Master", {
		formatter: f,
		reuseHandler: r,
		extHookAdjustObjectPageHeader: null,
		onInit: function () {
			var o, O = this.getView().byId("pageHeader"),
				c = (new sap.ui.core.Configuration()).getVersion(),
				v = new J({
					busy: true,
					delay: 0,
					localTime: null,
					showOnlyObjAttrRepTo: c.compareTo(1, 48) < 0 ? false : true
				});
			var sServiceUrl = "/sap/opu/odata/sap/HCMFAB_EMPLOYEELOOKUP_SRV/";
			var oModel = new sap.ui.model.odata.v2.ODataModel(sServiceUrl, false);
			this.setModel(oModel);

			this.getRouter().getRoute("master").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);
			o = this.getView().getBusyIndicatorDelay();
			this.setModel(v, "masterView");
			this.getOwnerComponent().getModel().metadataLoaded().then(function () {
				v.setProperty("/delay", o);
			});

			if (this.extHookAdjustObjectPageHeader) {
				this.extHookAdjustObjectPageHeader(O);
			}

			C.getDefaultAssignment().then(function (d) {
				this.getRouter().navTo("master", {
					EmployeeNumber: d.EmployeeId
				});

			}.bind(this));

		},
		onAfterRendering: function () {

		},
		onShowEmployeeDetails: function (e) {
			this.getRouter().navTo("master", {
				EmployeeNumber: e.getParameter("employeeNumber")
			}, false);
		},
		onShowDirectReports: function (e) {
			this.getRouter().navTo("object", {
				objectId: e.getParameter("employeeNumber")
			}, true);
		},

		onNavBackMaster: function () {
			var managerNumber = this.byId("managerNumber").getText();
			this.getRouter().navTo("master", {
				EmployeeNumber: managerNumber
			}, false);
		},

		onNavBack: function () {
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			oCrossAppNavigator.toExternal({
				target: {
					shellHash: "#Shell-home"
				}
			});

		},
		_onObjectMatched: function (e) {
			var E = e.getParameter("arguments").EmployeeNumber;
			this.getOwnerComponent().getModel().metadataLoaded().then(function () {
				this._bindView(E);
				var employeeNumber = E;
				var navButton = this.byId("navButtonMaster");
				C.getDefaultAssignment().then(function (d) {
					if (employeeNumber === d.EmployeeId) {
						navButton.setVisible(false);
					} else {
						navButton.setVisible(true);
					}
				});
			}.bind(this));
		},
		_bindView: function (e) {
			var v = this.getModel("masterView"),
				d = this.getModel();
			var o = "/" + d.createKey("EmployeeDetailSet", {
				EmployeeNumber: e
			});
			this.getView().bindElement({
				path: o,
				parameters: {
					expand: "toEmployeePicture,toManager/toEmployeePicture",
					operationMode: "Client"
				},
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						d.metadataLoaded().then(function () {
							v.setProperty("/busy", true);
						});
					},
					dataReceived: function (a) {
						v.setProperty("/busy", false);
					}.bind(this)
				}
			});
		},
		_onBindingChange: function () {
			var v = this.getView(),
				V = this.getModel("masterView"),
				e = v.getElementBinding();
			if (!e.getBoundContext()) {
				this.getRouter().getTargets().display("objectNotFound");
				return;
			}
			V.setProperty("/busy", false);
		},

		_crossAppResolver: function (m) {
				if (m) {
					var n = {
						target: {
							semanticObject: "Employee",
							action: "lookup"
						},
						params: {
							"EmployeeNumber": m
						}
					};
					return function () {
						return n;
					};
				}
				return null;
			}
			/*		_calcTime: function (o, t, v) {
						if (!o) {
							v.setProperty("/localTime", null);
							return;
						}
						var l, d, T;
						var a = D.getTimeInstance({
							style: "medium"
						});
						var b = D.getDateInstance({
							pattern: "EEEE"
						});
						var c = new Date();
						c.setMilliseconds(o + (c.getTimezoneOffset() * 60000));
						d = b.format(c);
						T = f.formatObjectTextWithBrackets(a.format(c), t);
						l = d + ", " + T;
						v.setProperty("/localTime", l);
					}*/

	});
});