/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"petrol/hr/ManagerProfile/controller/BaseController",
	"petrol/hr/ManagerProfile/utils/formatter",
	"petrol/hr/ManagerProfile/utils/reuseHandler",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter"
], function(BaseController, formatter, reuseHandler, JSONModel, Filter) {
	"use strict";

	return BaseController.extend("petrol.hr.ManagerProfile.blocks.DirectReportsBlockController", {

		formatter: formatter,
		reuseHandler: reuseHandler,

		/**
		 * Called when the controller is instantiated.
		 * @public
		 */
		onInit: function() {
			var oViewModel = new JSONModel({
				tableHeaderText: null
			});
			this.setModel(oViewModel, "directReportsView");

			this._oTable = this.byId("directReportsTable");
			this._oSearchfield = this.byId("directReportsSearch");

			// register event handler for the "refreshBlocks" event
			sap.ui.getCore().getEventBus().subscribe("petrol.hr.ManagerProfile", "refreshBlocks", this.onBlockRefresh, this);
		},

		onExit: function() {
			sap.ui.getCore().getEventBus().unsubscribe("petrol.hr.ManagerProfile", "refreshBlocks", this.onBlockRefresh, this);
		},

		onDirectReportsTableUpdateFinished: function(oEvent) {
			// update the counter information ontop of the table once the data is loaded 
			// and so the count is available
			var iCount = oEvent.getParameter("total");
			var sItemCountText = this.getModel("i18n").getResourceBundle().getText("tableItems", [iCount]);
			this.getView().getModel("directReportsView").setProperty("/tableHeaderText", sItemCountText);
		},

		/**
		 * Event handler when a table item gets pressed
		 * @param {sap.ui.base.Event} oEvent the table itemPress event
		 * @public
		 */
		onPress: function(oEvent) {
			/*
			 Delegate the eventing to the parent block.
			 The outside world will see this event as being triggered by the block itself.
			 */
			this.oParentBlock.fireShowEmployeeDetails({
				employeeNumber: oEvent.getSource().getBindingContext().getProperty("EmployeeNumber")
			});
		},

		onSearch: function(oEvent) {
			var oBinding = this._oTable.getBinding("items");
			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				oBinding.refresh();
			} else {
				var sSearch = oEvent.getParameter("query");
				if (sSearch && sSearch.length > 0) {
					// restrict the search results to the ones matching the text typed in the search box
					var oFilter = new Filter("EmployeeName", sap.ui.model.FilterOperator.Contains, sSearch);
					oBinding.filter(oFilter);
				} else {
					oBinding.filter();
				}
			}
		},

		onBlockRefresh: function() {
			var oBinding = this._oTable.getBinding("items");

			this._oSearchfield.setValue();
			this._bSearchTriggered = false;
			if (oBinding) {
				oBinding.filter();
			}
		}
	});
});