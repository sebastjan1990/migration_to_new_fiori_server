/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define([
	"sap/uxap/BlockBase"
], function(BlockBase) {
	"use strict";

	return BlockBase.extend("petrol.hr.ManagerProfile.blocks.DirectReportsBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "petrol.hr.ManagerProfile.blocks.DirectReportsBlock",
					type: "XML"
				},
				Expanded: {
					viewName: "petrol.hr.ManagerProfile.blocks.DirectReportsBlock",
					type: "XML"
				}
			},
			events: {
				showEmployeeDetails: {
					parameters: {
						employeeNumber: {}
					}
				}
			}
		}
	});
});