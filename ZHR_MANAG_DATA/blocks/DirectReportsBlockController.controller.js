/*
 * Copyright (C) 2009-2018 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["petrol/hr/ManagerProfile/controller/BaseController", "petrol/hr/ManagerProfile/utils/formatter",
	"petrol/hr/ManagerProfile/utils/reuseHandler", "sap/ui/model/json/JSONModel", "sap/ui/model/Filter"
], function (B, f, r, J, F) {
	"use strict";
	return B.extend("petrol.hr.ManagerProfile.blocks.DirectReportsBlockController", {
		formatter: f,
		reuseHandler: r,
		onInit: function () {
			var v = new J({
				tableHeaderText: null
			});
		/*	var sServiceUrl = "/sap/opu/odata/sap/HCMFAB_EMPLOYEELOOKUP_SRV/";
			var oModel = new sap.ui.model.odata.v2.ODataModel(sServiceUrl, false);
			this.setModel(oModel);*/
			this.setModel(v, "directReportsView");
			this._oTable = this.byId("directReportsTable");
			this._oSearchfield = this.byId("directReportsSearch");
			sap.ui.getCore().getEventBus().subscribe("petrol.hr.ManagerProfile", "refreshBlocks", this.onBlockRefresh, this);
		},
		onExit: function () {
			sap.ui.getCore().getEventBus().unsubscribe("petrol.hr.ManagerProfile", "refreshBlocks", this.onBlockRefresh, this);
		},
		onDirectReportsTableUpdateFinished: function (e) {
			var c = e.getParameter("total");
			var i = this.getModel("i18n").getResourceBundle().getText("tableItems", [c]);
			this.getView().getModel("directReportsView").setProperty("/tableHeaderText", i);
		},
		onPress: function (e) {
			this.oParentBlock.fireShowEmployeeDetails({
				employeeNumber: e.getSource().getBindingContext().getProperty("EmployeeNumber")
			});
		},
			onShowDetails: function (e) {
		/*	this.getRouter().navTo("object", {
					objectId : e.getSource().getBindingContext().getProperty("EmployeeNumber")*/
			this.oParentBlock.fireShowDirectReports({
				employeeNumber: e.getSource().getBindingContext().getProperty("EmployeeNumber")
			});	
		},
		
		onSearch: function (e) {
			var b = this._oTable.getBinding("items");
			if (e.getParameters().refreshButtonPressed) {
				b.refresh();
			} else {
				var s = e.getParameter("query");
				if (s && s.length > 0) {
					var o = new F("EmployeeName", sap.ui.model.FilterOperator.Contains, s);
					b.filter(o);
				} else {
					b.filter();
				}
			}
		},
		onBlockRefresh: function () {
			// var b = this._oTable.getBinding("items");
	/*		this._oSearchfield.setValue();
			this._bSearchTriggered = false;
			if (b) {
				b.filter();
			}*/
		}
	});
});