sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("petrol.hr.TimeStatemant.controller.View1", {
		
		onInit: function () {
			sap.ui.require(["hcm/fab/lib/common/util/CommonModelManager"], function(CommonModelManager) {
				CommonModelManager.getI18NModel().enhance({bundleName: "petrol.hr.TimeStatemant/messagebundle"});
			});
			
			var oMaxDate = new Date();
			
			oMaxDate.setUTCHours(0,0,0,0);
			
			var oViewModel = new sap.ui.model.json.JSONModel({
				maxDateOtherPeriod: oMaxDate,
				OtherPeriodSelected: false,
				selectedIndex: 1,
				otherPeriodValueState: null,
				otherPeriodValue: undefined,
				EmployeeId: null,
				oOtherPeriodMessage: null
			});
			
			this.getView().setModel(oViewModel, "viewModel");
			
			var oMessageProcessor = new sap.ui.core.message.ControlMessageProcessor();
			var oMessageManager  = sap.ui.getCore().getMessageManager();
			
			oMessageManager.registerMessageProcessor(oMessageProcessor);
			oMessageManager.registerObject(this.getView(), true);
			
			var oOtherPeriodRange = this.byId("OtherPeriodRange");
			var oValueBinding = oOtherPeriodRange.getBinding("value");
			
			try {
				
				var oOldFunction = oOtherPeriodRange._parseValue;
				
				oOtherPeriodRange._parseValue = function() {
					
					try {
					
						var oReturn = oOldFunction.apply(oOtherPeriodRange, arguments);
						
						if(oViewModel.getProperty("/oOtherPeriodMessage") != null) {	//eslint-disable-line
							oMessageManager.removeMessages([oViewModel.getProperty("/oOtherPeriodMessage")]);
							oViewModel.setProperty("/oOtherPeriodMessage", null);
							oMessageProcessor.checkMessages();
						}
						
						return oReturn;
						
					} catch(oError) {
						
						if (oError instanceof sap.ui.model.ParseException) {
							this.onOtherPeriodRangeParseError(oError);
						}
						
						throw oError;
						
					}
							
				}.bind(this);
				
			} catch(oError) {
				//do nothing
			}
			
			oValueBinding.getType().oConstraints = {
				maximum: oMaxDate
			};
			
			this.getEmployeeInfo();
			
		},
		
		getEmployeeInfo : function() {
			
			var oComponent = this.getOwnerComponent();
			var oEmployeeModel = oComponent.getModel("Employee");
			
			oEmployeeModel.metadataLoaded().then(function() {
				
				oEmployeeModel.read("/ConcurrentEmploymentSet", {
					success: function(oData) {
						
						var oViewModel = this.getView().getModel("viewModel");
						
						 var Emply = oViewModel.getProperty("/EmployeeId");
						if ( Emply === null) {
						for (var i = 0; i < oData.results.length; i++) {
							if (oData.results[i].IsDefaultAssignment === true) {
								oViewModel.setProperty("/EmployeeId", oData.results[i].EmployeeId);
								break;
							}
						}
						}
					}.bind(this)
				});
				
			}.bind(this));
			
		},
		
		onPeriodChange : function() {
			
			var oView = this.getView();
			var oViewModel = oView.getModel("viewModel");
			
			if(oViewModel.getProperty("/otherPeriodValueState") === "Error") {
				
				var oDateRange = this.byId("OtherPeriodRange");
				
				setTimeout(function() { oDateRange.onChange(); }, 0);
				
			}
		
		},
		
		onDownloadStatement : function() {
			
			var oView = this.getView();
			var oModel = oView.getModel();
			var oViewModel = oView.getModel("viewModel");
			var iSelectedIndex = oViewModel.getProperty("/selectedIndex");
			
			var oStartDate, oEndDate;
			
			if(iSelectedIndex === 0) {
				
				oStartDate = new Date();
				oStartDate.setUTCHours(0,0,0,0);
				oStartDate.setDate(oStartDate.getDate() - 7);
				
				oEndDate = new Date();
				oEndDate.setUTCHours(0,0,0,0);
				
			} else if(iSelectedIndex === 1) {
				
				oStartDate = new Date();
				oStartDate.setUTCHours(12,0,0,0);
				
				oStartDate.setDate(1);
				
				oEndDate = new Date();
				oEndDate.setUTCHours(12,0,0,0);
				
			} else {
				
				var oDateRange = this.byId("OtherPeriodRange");
				
				if(oViewModel.getProperty("/otherPeriodValueState") === "Error") {
					
					setTimeout(function() {
						oDateRange.focus();	
					}, 0);
					
					return;
				}
				
				oStartDate = oDateRange.getDateValue();
				oEndDate = oDateRange.getSecondDateValue();
				
				var iOffset = oStartDate.getTimezoneOffset();
				
				oStartDate.setMinutes(oStartDate.getMinutes() - iOffset);
				oEndDate.setMinutes(oEndDate.getMinutes() - iOffset);
				
			}
			
			var sTimeStatementUrl = oModel.createKey("/TimeStatementSet", {
				EmployeeID: oViewModel.getProperty("/EmployeeId"),
				Begda: oStartDate,
				Endda: oEndDate
			});
			
        	sTimeStatementUrl = this.getOwnerComponent().getModel().sServiceUrl + sTimeStatementUrl + "/$value";
        	
        	window.open(sTimeStatementUrl, "_blank");
			
		},
		
		onOtherPeriodRangeParseError : function(oParseException) {
			
			var oViewModel = this.getView().getModel("viewModel");
			
			var oMessageManager  = sap.ui.getCore().getMessageManager();
			var oMessageProcessor = new sap.ui.core.message.ControlMessageProcessor();
			
			var oOldMessage = oViewModel.getProperty("/oOtherPeriodMessage");
			
			if(oOldMessage != null) {	//eslint-disable-line
				oMessageManager.removeMessages(oOldMessage);
			}
			
			var oMessage = new sap.ui.core.message.Message({
				    message: oParseException.message,
				    type: sap.ui.core.MessageType.Error,
				    target: this.byId("OtherPeriodRange").getId() + "/value",
				    processor: oMessageProcessor
				 });
			
			oViewModel.setProperty("/oOtherPeriodMessage", oMessage);
			
			oMessageManager.addMessages(oMessage);
		},
		
		onAssignmentSwitch: function(oEvent) {
			var oViewModel = this.getView().getModel("viewModel");
			oViewModel.setProperty("/EmployeeId", oEvent.getParameter('selectedAssignment'));
		},
		
			onAssignmentsLoaded: function(oEvent) {
			var oViewModel = this.getView().getModel("viewModel");
			oViewModel.setProperty("/EmployeeId", oEvent.getParameter('defaultAssignment'));
		}
		
	});
});