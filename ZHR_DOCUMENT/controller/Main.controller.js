sap.ui.define([
	"petrol/hr/Document/controller/BaseController",
	"sap/m/PDFViewer",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"petrol/hr/Document/model/tableFilter"
], function (Controller, PDFViewer, JSONModel, Filter, tableFilter) {
	"use strict";

	return Controller.extend("petrol.hr.Document.controller.Main", {
		
		tableFilter : tableFilter,
		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
			
			var oViewModel = new JSONModel({
				busy : true,
				delay : 0,
				admin : true,
				showNavButton : false,
				personnelNo : "",
				documentsCount : 0,
				selectedIconTabBarKey : "all",
				selectedPersonnelNo : "",
				showSelectedPersonnel : false,
				count: {
					all : 0,
					FILTER1 : 0,
					FILTER2 : 0,
					FILTER3 : 0,
					FILTER4 : 0,
					FILTER5 : 0
				}
			});
			
			this.setModel(oViewModel, "mainView");
			
			this._oDocumentList = this.byId("documentList");
			this._bUpdateCounts = true; // by default update totals once
			
			this.getRouter().getRoute("main").attachPatternMatched(this._onObjectMatched, this);
			this.getRouter().getRoute("document").attachPatternMatched(this._onObjectMatchedDocument, this);
		},
		
		
		// Open dialog with filter/sort for Job List table
		onPressViewSettingsDocumentList : function() {
			this.tableFilter._getViewSettingsDialog("DocumentList", this._oDocumentList).open(); //.open("filter");
		},
		
		// Open PDF document
		onPressDocumentList : function(oEvent) {
			var sUrl = this.getModel().sServiceUrl + oEvent.getParameter("listItem").getBindingContextPath() + "/$value";
			var sTitle = oEvent.getParameter("listItem").getBindingContext().getProperty("DocumentName");
			var oPdf = this._getPdfViewer();
			oPdf.setSource(sUrl);
			oPdf.setTitle(sTitle);
			oPdf.open();
		},
		
		onUpdateFinishedDocumentList : function(oEvent) {
			var oModel = this.getModel("mainView");
			// Update total All if we are in all icon tab bar
			if ( oModel.getProperty("/selectedIconTabBarKey") === "all" && 
				 oEvent.getParameter("reason") === "Refresh") {
				oModel.setProperty("/count/all", oEvent.getParameter("total"));
			}
			oModel.setProperty("/documentsCount", oEvent.getParameter("total"));
		},
		
		// When Tab is changed
		onSelectIconTab : function(oEvent){
			var oBinding = this._oDocumentList.getBinding("items");
			var sKey = oEvent.getParameter("key");
			oBinding.filter(this._buildFilter(sKey));
			this._bUpdateCounts = false;
		},
		
		// When changed selection navigate to this Person's documents
		onChangePersonnelNo : function (oEvent) {
			var sPersonnelNo = oEvent.getParameter("selectedItem").getKey();
			var oQuery = {
				personnelNo : sPersonnelNo
			};
			this._bUpdateCounts = true;
			this.getRouter().navTo("main", {
				query : oQuery
			}, false );
		},
		
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			var oModel = this.getModel("mainView");
			var oArgs = oEvent.getParameter("arguments");
			var oQuery = oArgs["?query"];
			var sTab = "all";
			
			if (oQuery) {
				if (oQuery.personnelNo) {
					oModel.setProperty("/selectedPersonnelNo", oQuery.personnelNo);
					oModel.setProperty("/showSelectedPersonnel", true);
				} else {
					oModel.setProperty("/showSelectedPersonnel", false);
				}
				if (oQuery.tab){
					sTab = oQuery.tab;
				}
			}
			
			oModel.setProperty("/selectedIconTabBarKey", sTab);
			this._updateTotalCounts();
			this._loadData();
		},
		
		// Route used when called from Manager Profile application
		_onObjectMatchedDocument : function(oEvent) {
			var oModel = this.getModel("mainView");
			var sObjectId = oEvent.getParameter("arguments").objectId;
			oModel.setProperty("/selectedPersonnelNo", sObjectId);
			oModel.setProperty("/showSelectedPersonnel", true);
			oModel.setProperty("/selectedIconTabBarKey", "all");
			this._updateTotalCounts();
			this._loadData();
		},
		
		
		_updateTotalCounts : function() {
			var that = this;
			var oModel = this.getModel("mainView");
			var oDataModel = this.getModel();
			var sPath = this._oDocumentList.getBinding("items").getPath() + "/$count";
			
			// oDataModel.read(sPath, {
			// 	filters: [ that._buildFilter("all") ],
			// 	success : function(oData) {
			// 		oModel.setProperty("/count/all", oData);
			// 	}
			// })
			// Update totals for each filter type
			oDataModel.read(sPath, {
					filters: [ that._buildFilter("FILTER1") ],
				success : function(oData) {
					oModel.setProperty("/count/FILTER1", oData);
				}
			});
			oDataModel.read(sPath, {
					filters: [ that._buildFilter("FILTER2") ],
				success : function(oData) {
					oModel.setProperty("/count/FILTER2", oData);
				}
			});
			oDataModel.read(sPath, {
					filters: [ that._buildFilter("FILTER3") ],
				success : function(oData) {
					oModel.setProperty("/count/FILTER3", oData);
				}
			});
			oDataModel.read(sPath, {
					filters: [ that._buildFilter("FILTER4") ],
				success : function(oData) {
					oModel.setProperty("/count/FILTER4", oData);
				}
			});
			oDataModel.read(sPath, {
					filters: [ that._buildFilter("FILTER5") ],
				success : function(oData) {
					oModel.setProperty("/count/FILTER5", oData);
				}
			});
			
				
		},
		
		// Build filter to based on tab and user
		_buildFilter : function(sKey) {
			var oModel = this.getModel("mainView");
			var sPersonnelNo = oModel.getProperty("/selectedPersonnelNo");
			
			// Filter documents for specific person
			if (sPersonnelNo) {
				if (sKey !== "all") {
					// Add filter per tab
					var oFilter = new Filter({
						filters: [
							new Filter("Filter", "EQ", sKey),
							new Filter("PersonnelNo", "EQ", sPersonnelNo)
							],
							and: true
					});
				} else {
					var oFilter = new Filter("PersonnelNo", "EQ", sPersonnelNo);
				}
			} else {
				if (sKey !== "all") {
					oFilter = new Filter("Filter", "EQ", sKey);
				} else {
					oFilter = null;
				}
			}
			return oFilter;
		},
		
		// Load documents
		_loadData : function() {
			var oModel = this.getModel("mainView");
			
			var oBinding = this._oDocumentList.getBinding("items");
			if (oModel.getProperty("/selectedIconTabBarKey")) {
				var sKey = oModel.getProperty("/selectedIconTabBarKey");
			} else {
				sKey = "all";
			}
			
			// Filter documents based on selected tab and person
			oBinding.filter(this._buildFilter(sKey));
		},
		
		_getPdfViewer : function() {
			if (!this._oPdfViewer){
				this._oPdfViewer = new PDFViewer();
				this._oPdfViewer.setShowDownloadButton(false);
				this.getView().addDependent(this._oPdfViewer);
			}
			return this._oPdfViewer;
		}
		
	});
});