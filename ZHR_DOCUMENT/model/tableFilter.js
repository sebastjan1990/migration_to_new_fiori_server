//---------------------------------------------------------------------------------------------------
// Handle sorting and filtering of table
// At the moment only input field is suspported
//---------------------------------------------------------------------------------------------------
sap.ui.define([
		"sap/ui/model/Sorter",
		"sap/ui/model/Filter"
], function (Sorter, Filter) {
	"use strict";

	return {

			handleCancelViewSettings : function(oEvent) {
				
			},
			
			// Update sort and filtering
			handleConfirmViewSettings : function(oEvent) {
				var oTable = oEvent.getSource()._mTable;
				var mParams = oEvent.getParameters();
				var oBinding = oTable.getBinding("items");
				var aSorters = [];
				var aFilters = [];
				var sFilterText = ""; // Text to be displayed in info toolbar
				
				// Sort
				if (mParams.sortItem) {
					var sPath = mParams.sortItem.getKey();
					var bDescending = mParams.sortDescending;
					aSorters.push(new Sorter(sPath, bDescending));
					oBinding.sort(aSorters);
				}
				
				// Filter
				mParams.filterItems.forEach( function(oFilterItem) {
					var oFilter = new Filter(oFilterItem.getKey(), "Contains", oFilterItem.getCustomControl().getValue());
					aFilters.push(oFilter);
					sFilterText += oFilterItem.getText() + " (*" + oFilterItem.getCustomControl().getValue() + "*)  ";
				});
				// Set info toolbar when filtering
				if (aFilters.length > 0) {
					sFilterText = "Filtered by: " + sFilterText;
					oTable.getInfoToolbar().setVisible(true);
					oTable.getInfoToolbar().getContent()[0].setText(sFilterText);
				} else {
					oTable.getInfoToolbar().setVisible(false);
				}
				oBinding.filter(aFilters, "Application");
			},
			
			handleChangeInput : function(oEvent) {
				var sId = oEvent.getParameter("id");
				// Find custom control for this input field (we need it to set count)
				// Get dialog
				var oDialog = oEvent.getSource().getParent().getParent().getParent().getParent();
				
				// find NOT SUPPORTED BY IE11 - Using simple loop instead
				// var oCustomControl = oDialog.getFilterItems().find( function(oElement) {
				// 	return oElement.getCustomControl().getId() === sId;
				// });
				for (var i = 0; i < oDialog.getFilterItems().length; i++) {
					if (oDialog.getFilterItems()[i].getCustomControl().getId() === sId) {
						var oCustomControl = oDialog.getFilterItems()[i];
						break;
					}
				}
				if (oEvent.getParameter("newValue")) {
					oCustomControl.setFilterCount(1);
					oCustomControl.setSelected(true);
				} else {
					oCustomControl.setFilterCount(0);
					oCustomControl.setSelected(false);
				}
			},
			
			// Reset filters on table
			handleResetFilterViewSettings : function (oEvent) {
				// Remove filter counters
				var oDialog = oEvent.getSource();
				oDialog.getFilterItems().forEach( function(oCustomControl) {
					oCustomControl.setFilterCount(0);
					oCustomControl.setSelected(false);
					oCustomControl.getCustomControl().setValue("");
				});
			},
			
			// Create and Get dialog with sort/filter for specified table
			_getViewSettingsDialog : function (sTable, oTable) {
				
				var sDialog = "_oViewSettings" + sTable + "Dialog";
				if (!oTable._oViewSettingsDialog) {
					oTable._oViewSettingsDialog = sap.ui.xmlfragment("petrol.hr.Document.view.viewSettings.ViewSettings" + sTable, this);
					oTable.addDependent(oTable._oViewSettingsDialog);
					oTable._oViewSettingsDialog._mTable = oTable; // Keep reference to the table
				}
				return oTable._oViewSettingsDialog;
			}
		
	};
});