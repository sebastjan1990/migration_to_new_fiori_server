sap.ui.define([
	"petrol/hr/BasicProfile/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"petrol/hr/BasicProfile/utils/formatter"
], function (BaseController, JSONModel, MessageToast, MessageBox, formatter) {
	"use strict";

	return BaseController.extend("petrol.hr.BasicProfile.controller.Main", {
		
		formatter: formatter,
		
		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
			
			var oViewModel = new JSONModel({
				busy : true,
				delay : 0,
				editable : true,
				editMode : false,
				selectedPersonnelNo : "",
				uploadPhotoUrl : ""
			});
			
			
			this._oFileUploader = this.byId("fileUploader");
			
			this.setModel(oViewModel, "mainView");
			this.getRouter().getRoute("main").attachPatternMatched(this._onObjectMatched, this);
		},
		
		// Close popover and update phone number
		handleUpdateChanges : function() {
			var that = this;
			this.getModel().submitChanges({
				success: function(oData) {
					MessageToast.show(that.getResourceBundle().getText("textSuccessUpdate"));
				}
			});
			
		},
		
		// Change edit/display mode
		onPressEdit : function() {
			this.getModel("mainView").setProperty("/editMode", !this.getModel("mainView").getProperty("/editMode"));
		},
		
		// Cancel changes and change view mode
		onPressCancel : function() {
			this.getModel().resetChanges();
			var oFileUploader = this.byId("fileUploader");
			oFileUploader.setValue("");
			this.onPressEdit();
		},
		
		onPressSave : function() {
			//this.handleUpdateChanges();
			
			// Upload photo
			this._uploadFile();
			
			//this.getModel().refresh(true);
			// var PersonnelNo = this.getView().getBindingContext().getProperty("PersonnelNo");
			//var sPhotoPath = this.getModel().createKey("/PhotoSet", {PersonnelNo:"4134"} );

			
			
			//this.getModel().refresh(true);
		},
		
		handleUploadComplete : function(oEvent) {
			
			var PersonnelNo = this.getView().getBindingContext().getProperty("PersonnelNo");
			var sPhotoPath = this.getModel().createKey("/PhotoSet", {PersonnelNo:PersonnelNo});
			
			this.getModel().read(sPhotoPath, {
				success: function(oData) {
					
					// var oLayout = this.getView().byId("vBoxImage");
					
					// oLayout.removeAllItems();
					
					
					var oViewModel = this.getModel("mainView");
					
					oViewModel.setProperty("/PhotoURL", "");
					oViewModel.setProperty("/PhotoURL", oData.__metadata.media_src);
					oViewModel.setProperty("/editMode", false);
					
            		// var oFragment = sap.ui.xmlfragment("petrol.hr.BasicProfile.view.fragment.employeePicture",this);
            		
        			// oLayout.addItem(oFragment);
					
				}.bind(this)
			});
			
			//this.byId("avatarBig").setBusy(false);
			// this.getModel().refresh();
			// var sResponse = oEvent.getParameter("response");
			// if (sResponse) {
			// 	MessageBox.show("Return Code: " + sResponse, "Response", "Response");
			// }
		},
		
		// uploadFile : function() {
		// 	this.getModel().remove("/PhotoSet('4133')", {
		// 		//context: this.getView().getBindingContext(),
		// 		success: this._uploadFile.bind(this)
		// 	});
		// 	this.getModel().read(this.getView().getBindingContext().getPath(), {
		// 		urlParameters: {
		// 	        "$expand": "toPhoto"
		// 	    }
		// 	});
		// },
		
		_uploadFile : function() {
			var that = this;
		
			var oFileUploader = this._oFileUploader;
			if (oFileUploader.getValue()){
				//this.byId("avatarBig").setBusy(true);
				//Upload Path
				var sPath = this.getModel().sServiceUrl + this.getView().getBindingContext().getPath() + "/toPhoto";
				oFileUploader.setUploadUrl(sPath);
				this.getModel().refreshSecurityToken();
				oFileUploader.removeAllHeaderParameters();
				oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
				    name: "slug",
				    value: oFileUploader.getValue()
				}));
				oFileUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({
				    name: "x-csrf-token",
				    value: that.getModel().getSecurityToken()
				}));
				oFileUploader.upload();
			}
		},
		
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */
		
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			var that = this;
			
			this.getModel().read("/BasicProfileSet", {
				 urlParameters: {
			        "$expand": "AdditionalData"
			    },
				success: function(oData) {
					var oKey = {
						PersonnelNo : oData.results[0].PersonnelNo
					};
					var sObjectPath = that.getModel().createKey("/BasicProfileSet", oKey );
					that._bindView(sObjectPath, oData.results[0].PersonnelNo);
				}	
			});
		},
		
		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound
		 * @private
		 */
		_bindView : function (sObjectPath, PersonnelNo) {
			var that = this;
			var oViewModel = this.getModel("mainView"),
				oDataModel = this.getModel();

			this.getView().bindElement({
				path: sObjectPath,
				parameters: {
        				//expand: "toPhoto"
    				},
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						oDataModel.metadataLoaded().then(function () {
							// Busy indicator on view should only be set if metadata is loaded,
							// otherwise there may be two busy indications next to each other on the
							// screen. This happens because route matched handler already calls '_bindView'
							// while metadata is loaded.
							oViewModel.setProperty("/busy", true);
						});
					},
					dataReceived: function (oResult) {
//						that._bindView(sObjectPath);
						oViewModel.setProperty("/busy", false);
					}
				}
			});
			
			var sPhotoPath = oDataModel.createKey("/PhotoSet", {PersonnelNo:PersonnelNo} );
			
			oDataModel.read(sPhotoPath, {
				success: function(oData) {
					this.getModel("mainView").setProperty("/PhotoURL",oData.__metadata.media_src);
				}.bind(this)
			});
			// this.byId("photoGrid").bindElement({
			// 	path: sPhotoPath
			// });
		},

		_onBindingChange : function () {
			var	oElementBinding = this.getView().getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("notFound");
				return;
			}
			this.getModel("mainView").setProperty("busy",false);
		}
	});
});