sap.ui.define([
		"sap/ui/base/Object",
		"sap/m/MessageBox"
	], function (UI5Object, MessageBox) {
		"use strict";

		return UI5Object.extend("petrol.hr.BasicProfile.controller.ErrorHandler", {

			/**
			 * Handles application errors by automatically attaching to the model events and displaying errors when needed.
			 * @class
			 * @param {sap.ui.core.UIComponent} oComponent reference to the app's component
			 * @public
			 * @alias zsy_sst.controller.ErrorHandler
			 */
			 // aModel - array with models which should have error handler
			constructor : function (oComponent, aModel) {
				var that = this;
				this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();
				this._oComponent = oComponent;
				// this._oModel = oComponent.getModel();
				this._bMessageOpen = false;
				this._sErrorText = this._oResourceBundle.getText("errorText");
				
				// Attach error handler for each model
				aModel.forEach( function(oModel) {
					oModel.attachMetadataFailed(function (oEvent) {
						var oParams = oEvent.getParameters();
						that._showMetadataError(oParams.response);
					}, this);
	
					oModel.attachRequestFailed(function (oEvent) {
						var oParams = oEvent.getParameters();
	
						// An entity that was not found in the service is also throwing a 404 error in oData.
						// We already cover this case with a notFound target so we skip it here.
						// A request that cannot be sent to the server is a technical error that we have to handle though
						if (oParams.response.statusCode !== "404" || (oParams.response.statusCode === 404 && oParams.response.responseText.indexOf("Cannot POST") === 0)) {
							// Set error message to more user friendly
							if (oParams.response.responseText.substring(0,1) === "<"){
								
							} else {
								that._sErrorText = JSON.parse(oParams.response.responseText).error.message.value;
							};
							that._showServiceError(oParams.response);
						}
					}, this);
				})
			},

			/**
			 * Shows a {@link sap.m.MessageBox} when the metadata call has failed.
			 * The user can try to refresh the metadata.
			 * @param {string} sDetails a technical error to be displayed on request
			 * @private
			 */
			_showMetadataError : function (sDetails) {
				MessageBox.error(
					this._sErrorText,
					{
						id : "metadataErrorMessageBox",
						details: sDetails,
						styleClass: this._oComponent.getContentDensityClass(),
						actions: [MessageBox.Action.RETRY, MessageBox.Action.CLOSE],
						onClose: function (sAction) {
							if (sAction === MessageBox.Action.RETRY) {
								this._oModel.refreshMetadata();
							}
						}.bind(this)
					}
				);
			},

			/**
			 * Shows a {@link sap.m.MessageBox} when a service call has failed.
			 * Only the first error message will be display.
			 * @param {string} sDetails a technical error to be displayed on request
			 * @private
			 */
			_showServiceError : function (sDetails) {
				if (this._bMessageOpen) {
					return;
				}
				this._bMessageOpen = true;
				MessageBox.error(
					this._sErrorText,
					{
						id : "serviceErrorMessageBox",
						details: sDetails,
						styleClass: this._oComponent.getContentDensityClass(),
						actions: [MessageBox.Action.CLOSE],
						onClose: function () {
							this._bMessageOpen = false;
						}.bind(this)
					}
				);
			}
		});
	}
);