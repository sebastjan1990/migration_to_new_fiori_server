sap.ui.define([
], function() {
	"use strict";
	/*global document*/
	
	return {
		/**
		 * extracts the service path from the given absolute URL
		 * @public
		 * @param {string} sMediaSrc absolute URL
		 * @returns {string} service path
		 */
		formatImageURL: function(sMediaSrc) {
			var sUrl = "";
			if (sMediaSrc && typeof sMediaSrc === "string") {
				var oLink = document.createElement("a");
				oLink.href = sMediaSrc;
				sUrl = (oLink.pathname.charAt(0) === "/") ? oLink.pathname : "/" + oLink.pathname;
			}
			return sUrl + "?ver=" + new Date().getTime();
		}
	};
});